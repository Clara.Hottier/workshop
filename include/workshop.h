/*
** EPITECH PROJECT, 2019
** workshop
** File description:
** The program main header.
*/

#ifndef WORKSHOP_H
#define WORKSHOP_H

#ifndef EXIT_ERROR
#define EXIT_ERROR (84)
#endif

int my_function(int a);

#endif
